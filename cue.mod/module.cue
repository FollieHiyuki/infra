// SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

module: "gitlab.com/folliehiyuki/infra@v0"
language: {
	version: "v0.9.0"
}

package nx

#Project: {
	// JSON schema for Nx projects
	@jsonschema(schema="http://json-schema.org/draft-07/schema")
	@jsonschema(id="https://nx.dev/reference/project-configuration")

	// Named inputs used by inputs defined in targets
	namedInputs?: {
		[string]: #inputs
	}

	// Configures all the targets which define what tasks you can run
	// against the project
	targets?: {
		[string]: {
			// The function that Nx will invoke when you run this target
			executor?: string
			options?: {
				...
			}
			outputs?: [...string]

			// The name of a configuration to use as the default if a
			// configuration is not provided
			defaultConfiguration?: string

			// provides extra sets of values that will be merged into the
			// options map
			configurations?: {
				[string]: {
					...
				}
			}
			inputs?: #inputs
			dependsOn?: [...string | ({
				projects: _
				target:   _
				...
			} | {
				dependencies: _
				target:       _
				...
			} | {
				target: _
				...
			}) & {
				projects?: string | [...string]
				dependencies?: bool

				// The name of the target.
				target?: string

				// Configuration for params handling.
				params?: "ignore" | "forward" | *"ignore"
			}]

			// A shorthand for using the nx:run-commands executor
			command?: string

			// Specifies if the given target should be cacheable
			cache?: bool
			...
		}
	}
	tags?: [...string]
	implicitDependencies?: [...string]

	// Configuration for the nx release commands.
	release?: {
		// Configuration for the nx release version command.
		version?: {
			// The version generator to use. Defaults to
			// @nx/js:release-version.
			generator?: string

			// Options for the version generator.
			generatorOptions?: {
				...
			}
			...
		}
		...
	}

	#inputs: [...string | {
		// A glob used to determine a fileset.
		fileset?: string
	} | ({
		projects: _
		input:    _
		...
	} | {
		dependencies: _
		input:        _
		...
	} | {
		input: _
		...
	}) & {
		projects?: string | [...string]

		// Include files belonging to the input for all the project
		// dependencies of this target.
		dependencies?: bool

		// The name of the input.
		input?: string
	} | {
		// The command that will be executed and included into the hash.
		runtime?: string
	} | {
		// The env var that will be included into the hash.
		env?: string
	} | {
		// The list of external dependencies that our target depends on
		// for `nx:run-commands` and community plugins.
		externalDependencies?: [...string]
	} | {
		// The glob list of output files that project depends on.
		dependentTasksOutputFiles: string

		// Whether the check for outputs should be recursive or stop at
		// the first level of dependencies.
		transitive?: bool
	}]
	...
}

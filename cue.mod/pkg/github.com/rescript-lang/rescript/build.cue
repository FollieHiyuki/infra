package rescript

#Build: {
	// ReScript build configuration
	//
	// All paths are required to be in **Unix format** (foo/bar), the
	// build system normalizes them for other platforms internally
	@jsonschema(schema="http://json-schema.org/draft-04/schema#")

	// ReScript dependencies of the library, like in package.json.
	// Currently searches in `node_modules`
	"bs-dependencies"?: #dependencies

	// ReScript dev dependencies of the library, like in package.json.
	// Currently searches in `node_modules`
	"bs-dev-dependencies"?: #dependencies

	// (Not needed usually) external include directories, which will
	// be applied `-I` to all compilation units
	"bs-external-includes"?: [...string]

	// Flags passed to bsc.exe
	"bsc-flags"?: #["bsc-flags"]

	// Ignore generators, cut the dependency on generator tools
	"cut-generators"?: bool

	// Use the external stdlib library instead of the one shipped with
	// the compiler package
	"external-stdlib"?: string

	// (WIP) Pre defined rules
	generators?: [...#["rule-generator"]]

	// gentype config, see cristianoc/genType for more details
	gentypeconfig?: #["gentype-specs"]

	// a list of directories that bsb will not look into
	"ignored-dirs"?: [...string]

	// (Experimental) post-processing hook. bsb will invoke `cmd
	// ${file}` whenever a `${file}` is changed
	"js-post-build"?: #["js-post-build"]

	// Configuration for the JSX transformation.
	jsx?: #["jsx-specs"]

	// Package name
	name: string

	// can be true/false or a customized name
	namespace?: #["namespace-spec"]

	// ReScript can currently output to
	// [Commonjs](https://en.wikipedia.org/wiki/CommonJS), and [ES6
	// modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
	"package-specs"?: #["package-specs"]

	// Those dependencies are pinned (since version 8.4)
	"pinned-dependencies"?: #dependencies

	// preprocessors to pass to compiler. The syntax is
	// package_name/binary, for example: `pp/syntax.exe`. Currenly
	// searches in `node_modules`
	"pp-flags"?: #["pp-specs"]

	// PPX macros to pass to compiler. The syntax is
	// package_name/binary, for example:
	// `reason/reactjs_jsx_ppx_3.native`. Currenly searches in
	// `node_modules`
	"ppx-flags"?: #["ppx-specs"]

	// Configure reanalyze, a static code analysis tool for ReScript.
	reanalyze?: #reanalyze

	// ReScript comes with [Reason](http://reasonml.github.io/) by
	// default. Specific configurations here.
	reason?: #["reason-specs"]

	// Source code location
	sources: #sources
	suffix?: #["suffix-spec"]

	// Configuration for the uncurried mode.
	uncurried?: bool

	// (Experimental) whether to use the OCaml standard library.
	// Default: true
	"use-stdlib"?: bool

	// The semantic version of the ReScript library
	version?: string

	// warning numbers and whether to turn it into error or not
	warnings?: {
		error?: bool | string

		// Default: -40+6+7+27+32..39+44+45
		// [Here](https://caml.inria.fr/pub/docs/manual-ocaml/comp.html#sec270)
		// for the meanings of the warning flags
		number?: string
		...
	}

	#: "bs-dependency": string

	#: "bsc-flags": [...string] | {
		flags?: [...string]
		kind?: "reset" | "prefix" | "append"
		...
	}

	#: "build-generator": {
		edge?: [...string]
		name?: string
		...
	}

	#dependencies: [...#["bs-dependency"]]

	#: "gentype-specs": {
		path?: string
		...
	}

	#: "js-post-build": {
		cmd?: string
		...
	}

	#: "jsx-specs": {
		// JSX transformation mode
		mode?: "classic" | "automatic"

		// JSX module. Either "react" for React, or (since v11.1) any
		// valid module name to apply a generic JSX transform.
		module?: string

		// Build the given dependencies in JSX V3 compatibility mode.
		"v3-dependencies"?: #dependencies

		// Whether to apply the specific version of JSX PPX transformation
		version?: (3 | 4) & int
	}

	#: "module-format": "commonjs" | "es6" | "es6-global"

	#: "module-format-object": {
		// Default: false.
		"in-source"?: bool
		module:       #["module-format"]
		suffix?:      #["suffix-spec"]
		...
	}

	#: "namespace-spec": bool | string

	#: "package-spec": #["module-format"] | #["module-format-object"]

	#: "package-specs": [...#["package-spec"]] | #["package-spec"]

	#: "pp-specs": string

	#: "ppx-specs": [...string | [...string]]

	#: "react-jsx-version": number

	#reanalyze: {
		// The types of analysis to activate. `dce` means dead code
		// analysis, `exception` means exception analysis, and
		// `termination` is to check for infinite loops.
		analysis?: [..."dce" | "exception" | "termination"]

		// Paths for any folders you'd like to exclude from analysis.
		// Useful for bindings and similar. Example: `["src/bindings"]`.
		suppress?: [...string]

		// specify whether transitively dead items should be reported
		// (default: false)
		transitive?: bool

		// Any specific paths inside suppressed folders that you want to
		// unsuppress. Example: ["src/bindings/SomeBinding.res"].
		unsuppress?: [...string]
	}

	#: "reason-specs": {
		// Whether to apply the
		// [RescriptReact](https://github.com/rescript-lang/rescript-react)-specific
		// JSX PPX transformation.
		"react-jsx"?: #["react-jsx-version"]
		...
	}

	#: "rule-generator": {
		command?: string
		name?:    string
		...
	}

	#sourceItem: {
		// name of the directory
		dir: string
		files?: [...string] | {
			// Files to be excluded
			excludes?: [...string]

			// Regex to glob the patterns, syntax is documented
			// [here](http://caml.inria.fr/pub/docs/manual-ocaml/libref/Str.html),
			// for better incremental build performance, we'd suggest listing
			// files explicitly
			"slow-re"?: string
			...
		}

		// (WIP) Files generated in dev time
		generators?: [...#["build-generator"]]

		// Not implemented yet
		group?: string | {
			// When true, all subdirs are considered as a whole as dependency
			hierachy?: bool
			name?:     string
			...
		}
		"internal-depends"?: [...string]

		// Default: export all modules. It is recommended for library
		// developers to hide some files/interfaces
		public?: [...string] | "all"
		resources?: [...string]

		// Sub directories
		subdirs?: #sources | bool
		type?:    "dev"
		...
	} | string

	#sources: [...#sourceItem] | #sourceItem

	#: "suffix-spec": string
}

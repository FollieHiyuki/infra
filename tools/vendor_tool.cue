// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package tools

import (
	"path"

	"tool/exec"
	"tool/http"
)

command: "import-json-schemas": {
	getRescriptSchema: http.Get & {
		url: "https://raw.githubusercontent.com/rescript-lang/rescript-compiler/master/docs/docson/build-schema.json"
	}
	getNxProjectSchema: http.Get & {
		url: "https://raw.githubusercontent.com/nrwl/nx/master/packages/nx/schemas/project-schema.json"
	}

	// Output directories should exist before the files can be generated.
	// I'm too lazy to invoke mkDir here :)
	nx: exec.Run & {
		_outpath: path.FromSlash("../cue.mod/pkg/github.com/nrwl/nx/project.cue", "unix")

		stdin: getNxProjectSchema.response.body
		cmd:   "cue import -f -p nx -l #Project: -o \(_outpath) jsonschema: -"
	}
	rescript: exec.Run & {
		_outpath: path.FromSlash("../cue.mod/pkg/github.com/rescript-lang/rescript/build.cue", "unix")

		stdin: getRescriptSchema.response.body
		cmd:   "cue import -f -p rescript -l #Build: -o \(_outpath) jsonschema: -"
	}
}

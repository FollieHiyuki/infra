// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package generate

#Workspace: {
	name:   string
	path:   string
	pulumi: bool | *false
	dependencies: [...string] | *[]

	// Additional package.json and rescript.json config
	package:  #Package
	rescript: #ReScript
}

workspaces: [...#Workspace] & [
	{
		name: "bindings"
		path: "packages/bindings"
		package: {
			devDependencies: (_mapNpmDeps & {
				_deps: [
					"@pulumi/pulumi",
					"@pulumi/cloudflare",
				]
			}).out
			peerDependencies: devDependencies
		}
	},
	{
		name:   "cloudflare"
		path:   "stacks/cloudflare"
		pulumi: true
		dependencies: [
			"@pulumi/pulumi",
			"@pulumi/cloudflare",
		]
	},
]

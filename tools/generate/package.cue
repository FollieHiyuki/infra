// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package generate

import "github.com/nrwl/nx"

// Custom internal types
#dependency: [string]: string

// This struct acts like a convenient function to convert a list of dependency names
// into a map of dependencies and their versions defined in pnpm's default catalog
_mapNpmDeps: {
	_deps: [...string]

	out: #dependency & {
		for _, pkg in _deps {
			(pkg): "catalog:"
		}
	}
}

// https://json.schemastore.org/package.json is a pain to import into CUE, due to its external
// JSON Schema dependencies and invalid fields. Hence I only define what I need explicitly here.
#Package: {
	// Additional inputs
	_pulumi: bool

	// Optional fields I care to configure
	name:              string
	main?:             string
	dependencies?:     #dependency
	peerDependencies?: #dependency

	// Enforce ES6 style
	type: "module"

	// I don't intend to publish anything to NPM registry
	private: true

	// All subprojects are written in ReScript, so force devDependencies here
	devDependencies: (_mapNpmDeps & {
		_deps: ["@rescript/core", "rescript"]
	}).out

	// Make ReScript tasks available everywhere
	"nx": nx.#Project & {
		targets: {
			"res:build": {}
			"res:clean": {}
			"res:dev": {}
			"res:format": {}
		}
	}

	// All Pulumi projects here should work the same
	if _pulumi {
		main: "src/Main.res.js"
		dependencies: bindings: "workspace:*"
		"nx": targets: {
			preview: options: stack: name
			refresh: options: stack: name
			up: options: stack:      name
		}
	}
}

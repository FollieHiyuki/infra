// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package generate

import "github.com/rescript-lang/rescript"

#ReScript: rescript.#Build & {
	// Additional inputs
	_pulumi: bool

	// Recommended to be enabled by default
	namespace: bool | string | *true

	sources: {
		dir:     "src"
		subdirs: true
	}
	"package-specs": {
		module:      "es6-global"
		"in-source": true
	}
	suffix: ".res.js"

	// The rebuild of dependencies is handled by Nx, so
	// pinned-dependencies isn't specified here
	if _pulumi {
		"bs-dependencies": ["bindings", ...]
	}
}

// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package generate

import (
	"list"
	"strings"

	"encoding/json"
	"tool/exec"
	"tool/file"
)

command: "gen-project-conf": {
	gitRoot: exec.Run & {
		cmd: ["git", "rev-parse", "--show-toplevel"]
		stdout: string

		path: strings.TrimSpace(stdout)
	}

	for _, ws in workspaces {
		(ws.name): {
			// Ensure the below files can be written
			dir: file.MkdirAll & {
				path: "\(gitRoot.path)/\(ws.path)"
			}

			// Create package.json file
			package: file.Create & {
				filename:    "\(dir.path)/package.json"
				permissions: 0o644
				contents: json.Marshal(ws.package & {
					name:    ws.name
					_pulumi: ws.pulumi

					if list.MinItems(ws.dependencies, 1) {
						dependencies: (_mapNpmDeps & {
							_deps: ws.dependencies
						}).out
					}
				})
			}

			// Create rescript.json file
			rescript: file.Create & {
				filename:    "\(dir.path)/rescript.json"
				permissions: 0o644
				contents: json.Marshal(ws.rescript & {
					name:    ws.name
					_pulumi: ws.pulumi
				})
			}
		}
	}
}

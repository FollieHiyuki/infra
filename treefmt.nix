# SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

{ pkgs, ... }:
{
  projectRootFile = "flake.lock";

  # Custom formatters
  settings.formatter.rescript = {
    command = "${pkgs.nodePackages.pnpm}/bin/pnpm";
    options = [
      "nx"
      "run-many"
      "-t"
      "res:format"
    ];
    includes = [
      "*.res"
      "*.resi"
    ];
  };

  # Override settings for builtin formatters
  settings.formatter.biome.includes = [ "*.json" ];
  settings.formatter.yamlfmt.excludes = [ "pnpm-*.yaml" ];

  # List of builtin formatters: https://github.com/numtide/treefmt-nix/tree/main/programs
  programs =
    pkgs.lib.recursiveUpdate
      (builtins.listToAttrs (
        builtins.map
          (x: {
            name = x;
            value.enable = true;
          })
          [
            "cue"
            "biome"
            "nixfmt"
            "shellcheck"
            "shfmt"
            "statix"
            "taplo"
            "yamlfmt"
          ]
      ))
      {
        biome.settings.formatter = {
          indentStyle = "tab";
          indentWidth = 2;
        };
      };
}

# SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

{
  description = "folliehiyuki's infrastructure configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      treefmt-nix,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";

        treefmtEval = treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
      in
      {
        checks = with pkgs; {
          formatting = treefmtEval.config.build.check self;
          ansible-lint = runCommand "ansible-lint" { nativeBuildInputs = [ yamllint ]; } ''
            ${ansible-lint}/bin/ansible-lint ./playbooks/
            touch "$out"
          '';
          license = runCommand "reuse-lint" { } ''
            ${reuse}/bin/reuse lint
            touch "$out"
          '';
        };

        formatter = treefmtEval.config.build.wrapper;

        devShells.default =
          with pkgs;
          mkShellNoCC {
            shellHook = ''
              export PULUMI_SKIP_UPDATE_CHECK=true
              export PULUMI_AUTOMATION_API_SKIP_VERSION_CHECK=true
              export PULUMI_DISABLE_AUTOMATIC_PLUGIN_ACQUISITION=true
            '';
            inputsFrom = [ treefmtEval.config.build.devShell ];
            packages = [
              nodePackages.pnpm
              nodejs-slim
              pulumi
              pulumiPackages.pulumi-language-nodejs
              timoni
              fluxcd
            ];
          };
      }
    );
}
